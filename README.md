# Dark Wiby

## About
A dark theme with a pixel mascot for the web 1.0 search engine, wiby.me. I tried to make a simple stylesheet to keep within the spirit of the project. The image uses base64 encoding and the css file does not require any image files.

## Links
Userstyles.org page: https://userstyles.org/styles/148585/dark-wiby-with-pixel-mascot

## Screenshots
### Homepage
![homepage](wiby.png)

### Search Results
![search results](wiby2.png)

## Installation

### Step 1:
Install the Stylish or Stylus extension for Firefox or Chrome. Then, either:

### Step 2:
Install from userstyles (https://userstyles.org/styles/148585/dark-wiby-with-pixel-mascot)  
OR  
Import the *.css file into Stylish or Stylus manually